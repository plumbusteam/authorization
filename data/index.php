<?php
/**
 * Это важно! Заголовок должен отправляться сразу и именно в этом месте. Если заголовок отправлен не будет и произойдёт
 * ошибка, страница с ошибкой закешируется на время, прописанное в nginx по умолчанию.
 */
header('X-Accel-Expires: 0');
session_cache_limiter('public');
/**
 * При разработке должно быть включено - error_reporting(E_ALL), ini_set('display_errors', true)
 */
error_reporting(E_ALL);
ini_set('display_errors', true);

/**
 * Подключаем autoloader
 */
require_once __DIR__ . '/../vendor/autoload.php';
/**
 * используем настройки из .env файла
 */
(new \Dotenv\Dotenv(__DIR__ . '/../'))->load();

$configuration = [
    'networks' => [
        \Plumbus\Authorization\Social\SocialNetworkFactory::NETWORK_VKONTAKTE => [
            'enabled' => true,
            'app_id' => getenv('VK_APP_ID'),
            'app_secret' => getenv('VK_APP_SECRET'),
            'connect_url' => 'http://' . getenv('DOMAIN') . '/connect/vk'
        ],
        \Plumbus\Authorization\Social\SocialNetworkFactory::NETWORK_FACEBOOK => [
            'enabled' => true,
            'app_id' => getenv('FB_APP_ID'),
            'app_secret' => getenv('FB_APP_SECRET'),
            'default_graph_version' => 'v2.2',
            'connect_url' => 'http://' . getenv('DOMAIN') . '/connect/fb'
        ],
        \Plumbus\Authorization\Social\SocialNetworkFactory::NETWORK_ODNOKLASSNIKI => [
            'enabled' => true,
            'app_id' => getenv('OK_APP_ID'),
            'app_secret' => getenv('OK_APP_SECRET'),
            'app_public' => getenv('OK_APP_PUBLIC'),
            'connect_url' => 'http://' . getenv('DOMAIN') . '/connect/ok',
            /**
             * Для получения права VALUABLE_ACCESS приложения необходимо получать разрешение от модераторов Одноклассников.
             * Позволяет полуить ссылку на профиль пользователя в Одноклассниках, иначе ссылка будет пустой
             **/
            'extended_access' => true
        ],
    ]
];

/**
 * Создаем сервис
 */
$socialAuthorization = new \Plumbus\Authorization\Social\Service\SocialAuthorizationService();

/**
 * Конфигурируем
 */
$socialAuthorization->configure($configuration);

$requestUriParts = explode('/', $_SERVER['REQUEST_URI']);
$user = null;
switch ($requestUriParts[1]) {
    case 'connect':
        list($networkName, $paramaters) = explode('?', $requestUriParts[2]);
        switch ($networkName) {
            case 'vk':
                $user = $socialAuthorization->authorizeByGetRequest(
                    \Plumbus\Authorization\Social\SocialNetworkFactory::NETWORK_VKONTAKTE,
                    $_GET
                );
                break;
            case 'ok':
                $user = $socialAuthorization->authorizeByGetRequest(
                    \Plumbus\Authorization\Social\SocialNetworkFactory::NETWORK_ODNOKLASSNIKI,
                    $_GET
                );
                break;
            case 'fb':
                $user = $socialAuthorization->authorizeByGetRequest(
                    \Plumbus\Authorization\Social\SocialNetworkFactory::NETWORK_FACEBOOK,
                    $_GET
                );
                break;
        }
        $networks = $socialAuthorization->getEnabledNetworks();

        echo '<ul>';
        foreach ($networks as $network) {
            echo '<li><a href="' . $network->getAuthorizeLink() . '">' . $network->getName() . '</a>';
        }
        echo '</ul>';
        break;
}

if ($user) {
    echo '<pre>';
    print_r($user);
}
/**
 * Получаем все включенные способы социальной авторизации
 */
if ($_SERVER['REQUEST_URI'] == '/') {
    $networks = $socialAuthorization->getEnabledNetworks();

    echo '<ul>';
    foreach ($networks as $network) {
        echo '<li><a href="' . $network->getAuthorizeLink() . '">' . $network->getName() . '</a>';
    }
    echo '</ul>';
}
