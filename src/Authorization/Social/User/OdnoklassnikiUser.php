<?php namespace Plumbus\Authorization\Social\User;

class OdnoklassnikiUser extends SocialUser
{
    public function __construct(array $profile)
    {
        $this->setId((int) $profile['uid']);
        $this->setBirthDay((string) $profile['birthday']);
        $this->setAge((int) $profile['age']);
        $this->setFirstName((string) $profile['first_name']);
        $this->setLastName((string) $profile['last_name']);
        $this->setAvatarUrl((string) $profile['pic_3']);
        $this->setNickName((string) $profile['name']);
        if (isset($profile['url_profile'])) {
            $this->setUrl((string) $profile['url_profile']);
        }
        $sex = SocialUser::SEX_UNKNOWN;
        if ($profile['gender'] == 'male') {
            $sex = SocialUser::SEX_MALE;
        }

        if ($profile['gender'] == 'female') {
            $sex = SocialUser::SEX_FEMALE;
        }
        $this->setSex($sex);
    }
}
