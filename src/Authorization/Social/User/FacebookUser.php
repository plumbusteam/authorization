<?php namespace Plumbus\Authorization\Social\User;

class FacebookUser extends SocialUser
{
    public function __construct(array $profile)
    {
        $this->setId((int) $profile['id']);
        $sex = SocialUser::SEX_UNKNOWN;
        if ($profile['gender'] == 'male') {
            $sex = SocialUser::SEX_MALE;
        }

        if ($profile['gender'] == 'female') {
            $sex = SocialUser::SEX_FEMALE;
        }
        $this->setSex($sex);
        if (isset($profile['link'])) {
            $this->setUrl((string) $profile['link']);
        }
        $this->setFirstName((string) $profile['first_name']);
        $this->setLastName((string) $profile['last_name']);
        $this->setEmail((string) $profile['email']);
        $this->setAvatarUrl((string) $profile['picture']['data']['url']);
    }
}
