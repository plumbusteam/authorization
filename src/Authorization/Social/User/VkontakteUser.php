<?php namespace Plumbus\Authorization\Social\User;

class VkontakteUser extends SocialUser
{
    public function __construct(array $profile)
    {
        $this->setId((int) $profile['uid']);
        $this->setFirstName((string) $profile['first_name']);
        $this->setLastName((string) $profile['last_name']);
        $this->setNickName((string) $profile['nickname']);
        $this->setSex((int) $profile['sex']);
        if (!empty($profile['screen_name'])) {
            $this->setUrl('http://vk.com/' . $profile['screen_name']);
        } else {
            $this->setUrl('http://vk.com/id' . $profile['uid']);
        }

        $this->setAvatarUrl((string) $profile['photo_max']);
    }
}
