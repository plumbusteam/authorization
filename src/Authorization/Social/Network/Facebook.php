<?php namespace Plumbus\Authorization\Social\Network;

use Facebook\Exceptions\FacebookAuthenticationException;
use Facebook\Exceptions\FacebookResponseException;
use Facebook\Exceptions\FacebookSDKException;
use Plumbus\Authorization\Social\Network\Exception\SocialNetworkException;
use Plumbus\Authorization\Social\SocialNetworkFactory;
use Plumbus\Authorization\Social\User\FacebookUser;
use Plumbus\Authorization\Social\User\SocialUser;
use Facebook\Facebook as FacebookApi;

class Facebook extends AbstractNetwork
{
    /**
     * @var array
     */
    private $defaultPermissions = ['email'];

    /**
     * @var string
     */
    private $defaultFields = 'id,name,first_name,last_name,birthday,email,gender,link,picture';

    /**
     * @return string
     */
    public function getDefaultFields()
    {
        return $this->defaultFields;
    }

    /**
     * @return array
     */
    public function getDefaultPermissions()
    {
        return $this->defaultPermissions;
    }

    public function getNetworkId():int
    {
        return SocialNetworkFactory::NETWORK_FACEBOOK;
    }

    public function getAuthorizeLink():string
    {
        if (!isset($_SESSION) && !headers_sent()) {
            session_start();
        }

        $facebook = new FacebookApi([
            'app_id' => $this->getApplicationId(),
            'app_secret' => $this->getApplicationSecret()
        ]);

        $helper = $facebook->getRedirectLoginHelper();
        $permissions = $this->getDefaultPermissions();
        $loginUrl = $helper->getLoginUrl($this->getConnectUrl(), $permissions);

        return $loginUrl;
    }

    public function authorizeByRequest(array $request):SocialUser
    {
        if (!isset($_SESSION) && !headers_sent()) {
            session_start();
        }
        $_GET_ = isset($_GET) ? $_GET : null;
        $_GET = $request;


        $facebook = new FacebookApi([
            'app_id' => $this->getApplicationId(),
            'app_secret' => $this->getApplicationSecret()
        ]);

        $helper = $facebook->getRedirectLoginHelper();

        try {
            $accessToken = $helper->getAccessToken();
        } catch (FacebookResponseException $exception) {
            $_GET = $_GET_;
            throw new SocialNetworkException('Graph returns error ' . $exception->getCode());
        } catch (FacebookSDKException $exception) {
            $_GET = $_GET_;
            throw new SocialNetworkException('Graph validation fails ' . $exception->getCode());
        }

        $oAuth2Client = $facebook->getOAuth2Client();


        $tokenMetadata = $oAuth2Client->debugToken($accessToken);

        $tokenMetadata->validateAppId($this->getApplicationId());
        $tokenMetadata->validateExpiration();

        if (!$accessToken->isLongLived()) {
            // Exchanges a short-lived access token for a long-lived one
            try {
                $accessToken = $oAuth2Client->getLongLivedAccessToken($accessToken);
            } catch (FacebookSDKException $exception) {
                $_GET = $_GET_;
                throw new SocialNetworkException('Long lived token exchange fails: ' . $exception->getMessage());
            }
        }

        $profile = $facebook->get('/me?fields=' . $this->getDefaultFields(), $accessToken->getValue());
        $profile = json_decode($profile->getBody(), true);
        $_GET = $_GET_;
        return new FacebookUser($profile);
    }
}
