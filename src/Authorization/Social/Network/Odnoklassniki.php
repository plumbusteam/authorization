<?php namespace Plumbus\Authorization\Social\Network;

use GuzzleHttp\Client;
use GuzzleHttp\Exception\ClientException;
use Plumbus\Authorization\Social\Network\Exception\SocialNetworkException;
use Plumbus\Authorization\Social\Service\Exception\SocialAuthorizationException;
use Plumbus\Authorization\Social\SocialNetworkFactory;
use Plumbus\Authorization\Social\User\OdnoklassnikiUser;
use Plumbus\Authorization\Social\User\SocialUser;

class Odnoklassniki extends AbstractNetwork
{
    /**
     * @var string
     */
    private $defaultScope = 'VALUABLE_ACCESS';

    /**
     * @var string
     */
    private $extendedFields = 'url_profile';

    /**
     * @var string
     */
    private $oauthUrl = 'https://connect.ok.ru/oauth/authorize';

    /**
     * @var string
     */
    private $tokenRetrieveUrl = 'https://api.ok.ru/oauth/token.do';

    /**
     * @var string
     */
    private $apiUrl = 'https://api.ok.ru/fb.do';

    /**
     * @return string
     */
    public function getDefaultScope()
    {
        return $this->defaultScope;
    }

    /**
     * @return string
     */
    public function getExtendedFields()
    {
        return $this->extendedFields;
    }

    /**
     * @return int
     */
    public function getNetworkId():int
    {
        return SocialNetworkFactory::NETWORK_ODNOKLASSNIKI;
    }

    /**
     * @return string
     * @throws Exception\IllegalConfigurationException
     */
    public function getAuthorizeLink():string
    {
        $connectUrl = $this->getConnectUrl();
        $applicationId = $this->getApplicationId();
        $scope = urlencode($this->getScope() ?? $this->getDefaultScope());
        $link = $this->oauthUrl . '?client_id=' . $applicationId . '&scope=' . urlencode($scope) . '&response_type=code&redirect_uri=' . $connectUrl . '&layout=w';
        return $link;
    }

    private function hasExtendedAccess()
    {
        return (bool) ($this->getConfiguration()['extended_access'] ?? false);
    }

    public function authorizeByRequest(array $request):SocialUser
    {
        if (empty($request['code'])) {
            throw new SocialAuthorizationException('missed "code" parameter in request');
        }
        $code = $request['code'];

        $connectUrl = $this->getConnectUrl();
        $applicationId = $this->getApplicationId();
        $secret = $this->getApplicationSecret();
        $public = $this->getApplicationPublicKey();

        $url = $this->tokenRetrieveUrl . '?code=' . $code . '&client_id=' . $applicationId . '&client_secret=' . $secret . '&redirect_uri=' . $connectUrl . '&grant_type=authorization_code';

        $client = new Client();
        try {
            $response = $client->post($url);
        } catch (ClientException $exception) {
            throw new SocialNetworkException('Oauth2 odnoklassniki error: request failed with code ' . $exception->getCode());
        }

        $data = json_decode($response->getBody(), true);
        if (!is_array($data)) {
            throw new SocialNetworkException('Oauth2 odnoklassniki error: not json response ' . $response->getBody());
        }

        if (empty($data['access_token'])) {
            throw new SocialNetworkException('Oauth2 odnoklassniki error: empty access_token ' . $response->getBody());
        }

        $accessToken = $data['access_token'];
        $secret1 = md5($accessToken . $secret);
        $method = 'users.getCurrentUser';
        $string = 'application_key=' . $public . 'method=' . $method . $secret1;
        $sig = md5($string);

        $url = $this->apiUrl . '?application_key=' . $public . '&method=' . $method . '&access_token=' . $accessToken . '&sig=' . $sig;

        try {
            $response = $client->request('GET', $url);
        } catch (ClientException $exception) {
            throw new SocialNetworkException('API odnoklassniki error: request failed with code ' . $exception->getCode());
        }

        $data = json_decode($response->getBody(), true);
        if (!is_array($data)) {
            throw new SocialNetworkException('API odnoklassniki error: not json response ' . $method . $response->getBody());
        }

        if ($this->hasExtendedAccess()) {


            $method = 'users.getInfo';
            $string = 'application_key=' . $public . 'fields=' . $this->getExtendedFields() . 'method=' . $method . 'uids=' . $data['uid'] . $secret1;
            $sig = md5($string);


            $url = $this->apiUrl . '?application_key=' . $public . '&fields=' . $this->getExtendedFields() . '&method=' . $method . '&uids=' . $data['uid'] . '&access_token=' . $accessToken . '&sig=' . $sig;
            try {
                $response = $client->request('GET', $url);
            } catch (ClientException $exception) {
                throw new SocialNetworkException('API odnoklassniki error: request failed with code ' . $exception->getCode());
            }

            $extendedData = json_decode($response->getBody(), true);
            if (!is_array($extendedData)) {
                throw new SocialNetworkException('API odnoklassniki error: not json response ' . $method . $response->getBody());
            }


            $user = new OdnoklassnikiUser($data + reset($extendedData));
        } else {
            $user = new OdnoklassnikiUser($data);
        }
        return $user;
    }
}
