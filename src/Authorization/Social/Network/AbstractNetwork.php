<?php namespace Plumbus\Authorization\Social\Network;

use Plumbus\Authorization\Social\Network\Exception\IllegalConfigurationException;
use Plumbus\Authorization\Social\SocialNetworkFactoryTrait;
use Plumbus\Authorization\Social\User\SocialUser;

abstract class AbstractNetwork
{
    use SocialNetworkFactoryTrait;
    /**
     * @var array
     */
    private $configuration;

    /**
     * @return array
     */
    protected function getConfiguration()
    {
        return $this->configuration;
    }

    /**
     * @return string
     * @throws IllegalConfigurationException
     */
    protected function getConnectUrl()
    {
        if (empty($this->getConfiguration()['connect_url'])) {
            throw new IllegalConfigurationException('"connectUrl" is not filled for network ' . $this->getName());
        }

        return (string) $this->getConfiguration()['connect_url'];
    }

    /**
     * @return string
     * @throws IllegalConfigurationException
     */
    protected function getApplicationId()
    {
        if (empty($this->getConfiguration()['app_id'])) {
            throw new IllegalConfigurationException('"app_id" is not filled for network ' . $this->getName());
        }

        return (string) $this->getConfiguration()['app_id'];
    }

    /**
     * @return string
     * @throws IllegalConfigurationException
     */
    protected function getApplicationSecret()
    {
        if (empty($this->getConfiguration()['app_secret'])) {
            throw new IllegalConfigurationException('"app_secret" is not filled for network ' . $this->getName());
        }

        return (string) $this->getConfiguration()['app_secret'];
    }

    /**
     * @return string
     * @throws IllegalConfigurationException
     */
    public function getApplicationPublicKey()
    {
        if (empty($this->getConfiguration()['app_public'])) {
            throw new IllegalConfigurationException('"app_public" is not filled for network ' . $this->getName());
        }

        return (string) $this->getConfiguration()['app_public'];
    }

    /**
     * @return null|string
     */
    protected function getScope()
    {
        if (!empty($this->getConfiguration()['scope'])) {
            return (string) $this->getConfiguration()['scope'];
        }
        return null;
    }

    /**
     * @param array $configuration
     */
    public function setConfiguration($configuration)
    {
        $this->configuration = $configuration;
    }

    /**
     * @return string
     * @throws Exception\UnknownNetworkException
     */
    public function getName()
    {
        return $this->getSocialNetworkFactory()->getNetworkNameById($this->getNetworkId());
    }

    abstract public function getNetworkId():int;

    abstract public function getAuthorizeLink():string;

    abstract public function authorizeByRequest(array $request):SocialUser;

    public function configure(array $configuration)
    {
        $this->setConfiguration($configuration);
    }
}
