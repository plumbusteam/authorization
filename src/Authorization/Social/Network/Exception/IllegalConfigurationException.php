<?php namespace Plumbus\Authorization\Social\Network\Exception;

class IllegalConfigurationException extends SocialNetworkException
{
}
