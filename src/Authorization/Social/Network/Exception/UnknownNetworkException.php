<?php namespace Plumbus\Authorization\Social\Network\Exception;

class UnknownNetworkException extends SocialNetworkException
{
}
