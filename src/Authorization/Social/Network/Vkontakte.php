<?php namespace Plumbus\Authorization\Social\Network;

use GuzzleHttp\Client;
use GuzzleHttp\Exception\ClientException;
use Plumbus\Authorization\Social\Network\Exception\SocialNetworkException;
use Plumbus\Authorization\Social\Service\Exception\SocialAuthorizationException;
use Plumbus\Authorization\Social\SocialNetworkFactory;
use Plumbus\Authorization\Social\User\SocialUser;
use Plumbus\Authorization\Social\User\VkontakteUser;

class Vkontakte extends AbstractNetwork
{
    /**
     * @var string
     */
    private $defaultScope = 'friends,photos';

    /**
     * @var string
     */
    private $apiUrl = 'https://oauth.vk.com/access_token';

    /**
     * @var string
     */
    private $methodUrl = 'https://api.vk.com/method/users.get';

    /**
     * @var string
     */
    private $profileFields = 'uid,first_name,last_name,nickname,screen_name,photo_max,sex';

    /**
     * @return string
     */
    public function getProfileFields()
    {
        return $this->profileFields;
    }

    /**
     * @return string
     */
    public function getDefaultScope()
    {
        return $this->defaultScope;
    }

    /**
     * @return int
     */
    public function getNetworkId():int
    {
        return SocialNetworkFactory::NETWORK_VKONTAKTE;
    }

    /**
     * @return string
     * @throws Exception\IllegalConfigurationException
     */
    public function getAuthorizeLink():string
    {
        $connectUrl = $this->getConnectUrl();
        $applicationId = $this->getApplicationId();
        $scope = $this->getScope() ?? $this->getDefaultScope();
        $url = 'https://oauth.vk.com/authorize?client_id=' . $applicationId . '&redirect_uri=' . $connectUrl . '&scope=' . $scope . '&display=page';
        return $url;

    }

    /**
     * @param array $request
     * @return SocialUser
     * @throws Exception\IllegalConfigurationException
     * @throws SocialAuthorizationException
     * @throws SocialNetworkException
     */
    public function authorizeByRequest(array $request):SocialUser
    {
        if (empty($request['code'])) {
            throw new SocialAuthorizationException('missed "code" parameter in request');
        }
        $code = $request['code'];

        $connectUrl = $this->getConnectUrl();
        $applicationId = $this->getApplicationId();
        $secret = $this->getApplicationSecret();

        $url = $this->apiUrl . '?client_id=' . $applicationId . '&client_secret=' . $secret . '&code=' . $code . '&redirect_uri=' . $connectUrl;

        $client = new Client();
        try {
            $response = $client->request('GET', $url);
        } catch (ClientException $exception) {
            throw new SocialNetworkException('Oauth2 vkontakte error: request failed with code ' . $exception->getCode());
        }

        $data = json_decode($response->getBody(), true);
        if (!is_array($data)) {
            throw new SocialNetworkException('Oauth2 vkontakte error: not json response ' . $response->getBody());
        }

        $userId = $data['user_id'];
        $accessToken = $data['access_token'];

        $url = $this->methodUrl . '?uids=' . $userId . '&fields=' . $this->getProfileFields() . '&access_token=' . $accessToken;
        try {
            $response = $client->request('GET', $url);
        } catch (ClientException $exception) {
            throw new SocialNetworkException('API vkontakte error: request failed with code ' . $exception->getCode());
        }

        $data = json_decode($response->getBody(), true);
        if (!is_array($data)) {
            throw new SocialNetworkException('API vkontakte error: not json response ' . $response->getBody());
        }

        $userProfile = $data['response'][0];
        $user = new VkontakteUser($userProfile);
        return $user;
    }
}
