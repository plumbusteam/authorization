<?php namespace Plumbus\Authorization\Social\Service\Exception;

class NotConfiguredException extends SocialAuthorizationException
{
}
