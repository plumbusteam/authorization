<?php namespace Plumbus\Authorization\Social\Service;

trait SocialAuthorizationServiceTrait
{
    /**
     * @return SocialAuthorizationService
     */
    public function getSocialAuthorizationService()
    {
        return SocialAuthorizationService::instance();
    }
}
