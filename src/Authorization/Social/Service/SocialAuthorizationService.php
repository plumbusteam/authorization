<?php namespace Plumbus\Authorization\Social\Service;

use Plumbus\Authorization\Social\Network\AbstractNetwork;
use Plumbus\Authorization\Social\Network\Exception\SocialNetworkException;
use Plumbus\Authorization\Social\SocialNetworkFactoryTrait;
use Plumbus\Authorization\Social\Service\Exception\NotConfiguredException;
use Plumbus\Authorization\Social\User\SocialUser;
use Plumbus\Injectable\InjectableComponentTrait;

class SocialAuthorizationService
{
    use InjectableComponentTrait;
    use SocialNetworkFactoryTrait;

    /**
     * @var AbstractNetwork[]
     */
    private $networks;
    /**
     * @var bool
     */
    private $configured = false;

    /**
     * @var array
     */
    private $configuration = [];

    /**
     * @return boolean
     */
    public function isConfigured()
    {
        return $this->configured;
    }

    /**
     * @param boolean $configured
     */
    public function setConfigured(bool $configured)
    {
        $this->configured = $configured;
    }

    /**
     * @return array
     */
    public function getConfiguration()
    {
        return $this->configuration;
    }

    /**
     * @param array $configuration
     */
    public function setConfiguration(array $configuration)
    {
        $this->configuration = $configuration;
    }

    /**
     * @param array $configuration
     * @return $this
     */
    public function configure(array $configuration)
    {
        $this->setConfiguration($configuration);
        $this->setConfigured(true);
        return $this;
    }

    /**
     * @param int $networkId
     * @param array $getRequest
     * @return SocialUser
     * @throws NotConfiguredException
     * @throws SocialNetworkException
     */
    public function authorizeByGetRequest(int $networkId, array $getRequest):SocialUser
    {
        if (!$this->isConfigured()) {
            throw new NotConfiguredException();
        }

        $networks = $this->getEnabledNetworks();
        if (!isset($networks[$networkId])) {
            throw new SocialNetworkException('network with id ' . $networkId . ' is not found in enabled neworks');
        }

        return $networks[$networkId]->authorizeByRequest($getRequest);
    }

    /**
     * @return AbstractNetwork[]
     * @throws NotConfiguredException
     */
    public function getEnabledNetworks()
    {
        if (!$this->isConfigured()) {
            throw new NotConfiguredException();
        }

        $networks = [];

        foreach ($this->getConfiguration()['networks'] as $networkId => $networkConfiguration) {
            if (!empty($networkConfiguration['enabled'])) {
                $networks[$networkId] = $this->getNetwork((int) $networkId, $networkConfiguration);
            }
        }

        return $networks;
    }

    /**
     * @param int $networkId
     * @param array $networkConfiguration
     * @return AbstractNetwork
     * @throws Exception\SocialAuthorizationException
     */
    private function getNetwork(int $networkId, array $networkConfiguration)
    {
        if (!isset($this->networks[$networkId])) {
            $className = $this->getSocialNetworkFactory()->getNetworkClassById($networkId);
            $network = new $className;
            /**
             * @var AbstractNetwork $network
             */
            $network->configure($networkConfiguration);
            $this->networks[$networkId] = $network;
        }
        return $this->networks[$networkId];
    }
}
