<?php namespace Plumbus\Authorization\Social;

trait SocialNetworkFactoryTrait
{
    /**
     * @return SocialNetworkFactory
     */
    public function getSocialNetworkFactory()
    {
        return SocialNetworkFactory::instance();
    }
}
