<?php namespace Plumbus\Authorization\Social;

use Plumbus\Authorization\Social\Network\Exception\UnknownNetworkException;
use Plumbus\Authorization\Social\Network\Facebook;
use Plumbus\Authorization\Social\Network\Odnoklassniki;
use Plumbus\Authorization\Social\Network\Vkontakte;
use Plumbus\Injectable\InjectableComponentTrait;

class SocialNetworkFactory
{
    const NETWORK_VKONTAKTE = 1;
    const NETWORK_FACEBOOK = 2;
    const NETWORK_ODNOKLASSNIKI = 3;

    private $networks = [
        self::NETWORK_VKONTAKTE => [
            'class' => Vkontakte::class,
            'name' => 'vkontakte',
        ],
        self::NETWORK_FACEBOOK => [
            'class' => Facebook::class,
            'name' => 'facebook',
        ],
        self::NETWORK_ODNOKLASSNIKI => [
            'class' => Odnoklassniki::class,
            'name' => 'odnoklassniki',
        ],
    ];

    use InjectableComponentTrait;

    /**
     * @param $networkId
     * @return string
     * @throws UnknownNetworkException
     */
    public function getNetworkClassById(int $networkId):string
    {
        if (!isset($this->networks[$networkId])) {
            throw new UnknownNetworkException('networkId: ' . $networkId);
        }

        return $this->networks[$networkId]['class'];
    }

    /**
     * @param int $networkId
     * @return string
     * @throws UnknownNetworkException
     */
    public function getNetworkNameById(int $networkId):string
    {
        if (!isset($this->networks[$networkId])) {
            throw new UnknownNetworkException('networkId: ' . $networkId);
        }

        return $this->networks[$networkId]['name'];
    }
}
